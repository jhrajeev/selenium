package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import week6.day1.LearnDataProvider;

public class TC002CreateLeads extends ProjectMethods{

	@BeforeTest (groups = {"smoke"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "Rajeev";
		category = "smoke";
	}

	@Test (groups = {"smoke"}, dataProvider = "qa", dataProviderClass = LearnDataProvider.class)
	public void createLeads(String cName, String fName, String lName, String email, String mobileNum) {
		/*		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
		click(locateElement("partiallink", "CRM/SFA"));
		click(locateElement("LinkText", "Create Lead"));
		type(locateElement("id", "createLeadForm_companyName"), cName);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);
		type(locateElement("id", "createLeadForm_primaryEmail"), email);
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ""+mobileNum);

		//click(locateElement("xpath", "//img[@src='/images/fieldlookup.gif']"));

		selectDropDownUsingText (locateElement("id", "createLeadForm_industryEnumId"), "Computer Software");


		click(locateElement("name", "submitButton"));


	}


}
