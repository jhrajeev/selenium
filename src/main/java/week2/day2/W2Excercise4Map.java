package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class W2Excercise4Map {

	public static void main(String[] args) {
		String company = "RedgrapeBusinessServices";
		char[] comp = company.toCharArray();

		//System.out.println(comp);
		int value =0;
		Map<Character, Integer> map = new HashMap<>();
		for (char c : comp) {
			if (map.containsKey(c)) {
				value = map.get(c);
				map.put(c, value+1);
			} else {
				map.put(c, 1);
			}
		}

		System.out.println(map);

	}

}
