package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class W2Excercise3 {
	public static void main(String[] args) {
		Set<Integer> st = new HashSet<>();
		st.add(3);
		st.add(2);
		st.add(7);
		st.add(4);
		st.add(9);
		
		
		List<Integer> ls = new ArrayList<>();
		ls.addAll(st);
		System.out.println(ls);

		for (int i = 0; i < ls.size(); i++) {
			if (i%2 == 0) {
				System.out.println(ls.get(i));
			}
		}
		
	}

}
