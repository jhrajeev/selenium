package week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class W2Excercise2 {
	public static void main(String[] args) {
		List<Integer> ls = new ArrayList<>();
		ls.add(1);
		ls.add(2);
		ls.add(3);
		ls.add(4);
		//System.out.println(ls);
		
		for (int i = 0; i < ls.size(); i++) {
			System.out.println(ls.get(i));
		}
		
		//Collections.reverse(ls);
		
		int j = ls.size();
		for (int i = j-1; i >=0 ; i--) {
			System.out.println(ls.get(i));
		}
		
	
		
	}
	

}
