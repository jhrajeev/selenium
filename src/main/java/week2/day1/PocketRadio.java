package week2.day1;

public class PocketRadio extends Radio {
	
	//Implement the abstract method
	public void changeBattery() {
		System.out.println("Changed the Battery");
	}
	
	public static void main(String[] args) {
	
		// Create Object of Pocket Radio and access both methods (from Radio) 
		PocketRadio pd = new PocketRadio();
		pd.changeBattery();
		pd.playStation();
	}

}

