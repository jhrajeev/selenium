package week2.day1;

public interface Entertainment {
	
	public void turnON();
	public void turnOff();
	public void changeChannels();
	public void playMusic();
		

}