package week2.day1;

public abstract class Radio {
	
	//Implement 
	public void playStation() {
		System.out.println("Play Station set");
	}
	
	//Abstract Method
	public abstract void changeBattery();
	

	/*// Expected Error
	public static void main(String[] args) {
		Radio rd = new Radio();
		
	}*/
	
}
