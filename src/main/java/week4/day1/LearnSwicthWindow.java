package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnSwicthWindow {

	public static void main(String[] args) throws IOException {
		// Launch the browser
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		//Enter the URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		
		// Click Contact us
		driver.findElementByLinkText("Contact Us").click();
		
		//Take set of widows reference
		Set<String> wins = driver.getWindowHandles();
		
		//Convert to List
		List<String> winList = new ArrayList<>();
		winList.addAll(wins);
		
		//Switch to 2nd Window
		driver.switchTo().window(winList.get(1));
		
		//Take screen shot
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File ("./snaps/img1.jpg");
		FileUtils.copyFile(src, obj);
			
		//Close the 1st WIndow
		
		driver.switchTo().window(winList.get(0));
		driver.close();
		
		//Take control of 2nd window
		driver.switchTo().window(winList.get(1));
		
		//Get Title of the page
		String title = driver.getTitle();
		System.out.println("Title of the page is: "+title);
		
		
	}

}
