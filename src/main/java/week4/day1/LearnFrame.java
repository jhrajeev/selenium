package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {
		
		
			// Launch the browser
			System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();

			//Enter the URL
			driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
			driver.manage().window().maximize();
			
			//Enter the frame
			driver.switchTo().frame("iframeResult");
			driver.findElementByXPath("//button[text() = 'Try it']").click();
			
			// Enter value in Alert
			Alert alertRef = driver.switchTo().alert();
			alertRef.sendKeys("Rajeev");
			
			
			//Click Ok in alert
			alertRef.accept();
			
			//Validate the message
			String name = driver.findElementByXPath("//p[@id = 'demo']").getText();
			if (name.contains("Rajeev")) {
				System.out.println("Validation for name is a Pass.");
				
			//Come to the default Frame
			
			driver.switchTo().defaultContent();
			driver.findElementByXPath("//a[@title = 'Change Orientation']").click();
				
			}
		
		

	}

}
