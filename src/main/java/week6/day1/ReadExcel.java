package week6.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {

	@Test
	public static Object[][] readExcel() throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		int cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		
		Object[][] data = new Object[rowCount][cellCount];
		
		for (int i = 1; i <= rowCount; i++) {
			XSSFRow row = sheet.getRow(i);

			for (int j = 0; j < cellCount; j++) {
				XSSFCell cell = row.getCell(j);

				try {
					String value = cell.getStringCellValue();
					data[i-1][j] = value;
					System.out.println(value);
				} catch (NullPointerException e) {
					System.out.println("");
				}
			}
		} 
		wb.close();
		return data;
	}


}
