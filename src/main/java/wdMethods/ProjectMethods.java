package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;


public class ProjectMethods extends SeMethods{
	
	
	@BeforeSuite (groups = {"any"})
	public void beforeSuite() {
		startResult();
	}
	
@BeforeMethod (groups = {"any"})
	public void login() {
		beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);		
	}
	
	@AfterMethod (groups = {"any"})
	public void closeApp() {
		closeBrowser();
	}
/*	@AfterClass (groups = {"any"})
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest (groups = {"any"})
	public void afterTest() {
		System.out.println("@AfterTest");
	}*/
	
	
	@AfterSuite (groups = {"any"})
	public void afterSuite() {
		endResult();
	}


}






