package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginAndLogout {

	public static void main(String[] args) {
		// Launch the browser
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		//Enter the URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();		
		
		//Enter Login CRedentials
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		//Click CRM/SFA
		driver.findElementByLinkText("CRM/SFA").click();
		
		//Click on Create leads
		driver.findElementByLinkText("Create Lead").click();
		
		//Fill the form and create
		driver.findElementById("createLeadForm_companyName").sendKeys("RGBS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Rajeev");
		driver.findElementById("createLeadForm_lastName").sendKeys("Rajeev");
			
		//Using selectbyvisibletext select Public relation from the dropdown
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");
		
		//select Automobile using select by value
		WebElement mc = driver.findElementById("createLeadForm_marketingCampaignId");
		Select m = new Select(mc);
		m.selectByValue("CATRQ_AUTOMOBILE");
		
		//Print All values from 'Industry'
		WebElement ind = driver.findElementById("createLeadForm_industryEnumId");
		Select in = new Select(ind);
		List<WebElement> ind_Options = in.getOptions();
		for (WebElement io : ind_Options) {
			String text = io.getText();
			//System.out.println(io.getText());
			
			if (text.startsWith("M")) {
				System.out.println(text);
				
			}
					
		} 
		
		
		//Print All values starts by M
	
		
		
		//driver.findElementByName("submitButton").click();
		
		//driver.close();		
		

	}

}
