package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Day1ExeciseIRCTC {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub


		// Launch the browser
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		//Enter the URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();

		/*		//Print all Vailable links in the page
		List<WebElement> allLinks = driver.findElementsByTagName("a");
		int totalLinks = allLinks.size();
		System.out.println("All Lniks in the page are: "+totalLinks);

		//Click on Third Link
		WebElement thirdLink = allLinks.get(2);
		thirdLink.click();
		 */

		//FIll the sign-up form
		driver.findElementById("userRegistrationForm:userName").sendKeys("raj20120");
		driver.findElementById("userRegistrationForm:password").sendKeys("rajeev2012!");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("rajeev2012!");

		//Select the drop down
		WebElement secQ = driver.findElementById("userRegistrationForm:securityQ");
		Select sq = new Select(secQ);
		sq.selectByVisibleText("What was the name of your first school?");

		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("rajeev2012!");

		WebElement lan = driver.findElementById("userRegistrationForm:prelan");
		Select pl = new Select(lan);
		pl.selectByValue("en");

		driver.findElementById("userRegistrationForm:firstName").sendKeys("Kumar");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Rajeev");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Ranjan");

		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();

		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select d = new Select(day);
		d.selectByVisibleText("23");

		WebElement mon = driver.findElementById("userRegistrationForm:dobMonth");
		Select m = new Select(mon);
		m.selectByVisibleText("OCT");		

		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select y = new Select(year);
		y.selectByVisibleText("1986");		


		WebElement occ = driver.findElementById("userRegistrationForm:occupation");
		Select o = new Select(occ);
		o.selectByVisibleText("Private");	


		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select cou = new Select(country);
		cou.selectByVisibleText("India");


		driver.findElementById("userRegistrationForm:email").sendKeys("abb.raj2310@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9595959595");


		WebElement nat = driver.findElementById("userRegistrationForm:nationalityId");
		Select na = new Select(nat);
		na.selectByVisibleText("India");

		driver.findElementById("userRegistrationForm:address").sendKeys("Jains Abhisek Apt, Tambaram");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600073", Keys.TAB);
		Thread.sleep(5000);

		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select ci = new Select(city);
		ci.selectByVisibleText("Kanchipuram");	
		Thread.sleep(5000);

		WebElement ps = driver.findElementById("userRegistrationForm:postofficeName");
		Select po = new Select(ps);
		po.selectByVisibleText("Selaiyur S.O");		

		driver.findElementById("userRegistrationForm:landline").sendKeys("9898989898");
		driver.findElementById("userRegistrationForm:resAndOff:0").click();



		driver.findElementById("userRegistrationForm:newsletter:1").click();
		driver.findElementById("userRegistrationForm:commercialpromo:1").click();
		driver.findElementById("userRegistrationForm:irctcsbicard:1").click();

		//driver.close();

	}

}
