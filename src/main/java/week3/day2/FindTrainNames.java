package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindTrainNames {

	public static void main(String[] args) throws InterruptedException {
		
		// Launch the browser
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		//Enter the URL
		driver.get("https://erail.in/");
		driver.manage().window().maximize();		
		
		//ENter From and TO
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC", Keys.TAB);
		
		WebElement ele = driver.findElementById("chkSelectDateOnly");
		if (ele.isSelected()) {
			ele.click();
			
		}
		
		Thread.sleep(2000);
		
		//Table
		WebElement table = driver.findElementByXPath("//table[@class = 'DataTable DataTableHeader TrainList']");
		
		// Number of Rows
		List<WebElement> allRows = table.findElements(By.tagName("tr"));
		
		
		//Increment to next rows
		for (int i = 0; i < allRows.size(); i++) {
			List<WebElement> allColumn = allRows.get(i).findElements(By.tagName("td"));
			String text = allColumn.get(1).getText();
			System.out.println(text);
		}
			
			
	}
			
}

