package week1.day1;

import java.util.Scanner;

public class LearnSwitch {	
	public void callSwitch(){
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter the value1: ");
			int value1 = scan.nextInt();
			System.out.println("Enter the value2: ");
			int value2 = scan.nextInt();
			System.out.println("Enter the option: ");
			String option = scan.next();
			
			switch (option) {
			case ("add"):
				System.out.println("Add two Value: " +(value1 + value2));
				break;
			case ("sub"):
				System.out.println("sub two value: " +(value1 - value2));
				break;
			
			default:
				System.out.println("Invalid input");
				break;
			}
	}

public static void main(String[] args) {
	LearnSwitch LC = new LearnSwitch();
	LC.callSwitch();
	}
}