package week1.day2;

import java.util.Scanner;

public class OddNumberArray {
	
	public static void main(String[] args) {
		
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the Array size: ");
		int size = sc.nextInt();
		
		int numbers[] = new int [size];
		for (int i=0; i<size; i++) {
			numbers[i] = sc.nextInt();	
		}
		
		//int numbers[] = {1,5,4,8,3};
		int sum =0;
		for (int i : numbers) {
			if (i%2!=0) {
				sum = sum+i ;
			}
		}
		System.out.println(sum);
	}
		
}

